"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var Discord = require("discord.js");
var cron = require("node-cron");
var minimist = require("minimist");
var moment = require("moment");
var string_argv_1 = require("string-argv");
var botToken = "ODQ1NTkzNjUzNDg5MzAzNTYy.YKjOdQ.G3hx4gVf1Gus_-3it22SXcW4r48";
var client = new Discord.Client();
var prefix = "r!";
var dateRegex = /(\d{1,2})\/(\d{1,2})(\/(\d{4}))?/gm;
var timeRegex = /(\d{1,2}h)?(\d{1,2})?m?/gm;
var badCommand = "Commande incorrecte: tapez r! --help pour afficher l'aide :)";
var thumbsUp = "👍";
var thumbsDown = "👎";
/*  Commandes

******** Rappel dans X minutes ********
* r! "Mon rappel" -t 30m   -> rappel dans 30 minutes
* r! "Mon rappel" -t 1h    -> rappel dans 1 heure
* r! "Mon rappel" -t 1h30  -> rappel dans 1h30

******** Rappel à une heure définie ********
* r! "Mon rappel" -h 16h25          -> rappel aujourd'hui à 16h25
* r! "Mon rappel" -d DD/MM/YYYY -h 16h    -> rappel le DD/MM/YYYY à 16h00
* r! "Mon rappel" -d DD/MM/YYYY -h 16h30  -> rappel le DD/MM/YYYY à 16h30

******** Liste des rappels ********
* r! -list   -> liste les rappels
*/
client.login(botToken);
client.on("message", function (message) { return __awaiter(void 0, void 0, void 0, function () {
    var authorUsername, commandBody, args, scheduleDate_1, cronString_1, scheduleMessage_1, reminderMessage_1, day, monthIndex, year, hours, minutes, currentDate, currentYear, timeMatches, m, timeMatches, m, dateMatches, askScheduleMessage, askSchedule, filter, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                if (message.author.bot || !message.content.startsWith(prefix))
                    return [2 /*return*/];
                authorUsername = message.author.username;
                commandBody = message.content.slice(prefix.length);
                _a.label = 1;
            case 1:
                _a.trys.push([1, 3, , 4]);
                args = getArgs(commandBody);
                scheduleMessage_1 = args._[0];
                day = void 0;
                monthIndex = void 0;
                year = void 0;
                hours = 0;
                minutes = 0;
                currentDate = new Date();
                currentYear = currentDate.getFullYear();
                if (args.t) {
                    timeMatches = timeRegex.exec(args.t);
                    if (timeMatches === null) {
                        throw badCommand;
                    }
                    if (timeMatches[0].endsWith("h")) {
                        hours = parseInt(timeMatches[0].slice(0, -1));
                        if (!isBetween(hours, 0, 23)) {
                            throw "Horaire incorrect: " + timeMatches[0];
                        }
                        if (timeMatches.length == 2) {
                            minutes = parseInt(timeMatches[1]);
                            if (!isBetween(minutes, 0, 59)) {
                                throw "Horaire incorrect: " + timeMatches[1];
                            }
                        }
                    }
                    else {
                        m = timeMatches[0].endsWith("m")
                            ? timeMatches[0].slice(0, -1)
                            : timeMatches[0];
                        minutes = parseInt(m);
                        if (!isBetween(minutes, 0, 59)) {
                            throw "Horaire incorrect: " + timeMatches[0];
                        }
                    }
                    scheduleDate_1 = moment(new Date())
                        .add(hours, "h")
                        .add(minutes, "m")
                        .toDate();
                }
                else if (args.h) {
                    timeMatches = timeRegex.exec(args.h);
                    if (timeMatches === null) {
                        throw badCommand;
                    }
                    if (timeMatches[0].includes("h")) {
                        hours = parseInt(timeMatches[0].slice(0, -1));
                        if (!isBetween(hours, 0, 23)) {
                            throw "Horaire incorrect: " + timeMatches[0];
                        }
                        if (timeMatches.length == 2) {
                            minutes = parseInt(timeMatches[1]);
                            if (!isBetween(minutes, 0, 59)) {
                                throw "Horaire incorrect: " + timeMatches[1];
                            }
                        }
                    }
                    else {
                        m = timeMatches[0].endsWith("m")
                            ? timeMatches[0].slice(0, -1)
                            : timeMatches[0];
                        minutes = parseInt(m);
                        if (!isBetween(minutes, 0, 59)) {
                            throw "Horaire incorrect: " + timeMatches[0];
                        }
                    }
                    if (args.d) {
                        dateMatches = dateRegex.exec(args.d);
                        if (dateMatches === null) {
                            throw "Date incorrecte";
                        }
                        else {
                            day = parseInt(dateMatches[0]);
                            monthIndex = parseInt(dateMatches[1]) - 1;
                            year =
                                dateMatches.length === 3 ? parseInt(dateMatches[2]) : currentYear;
                            if (year !== currentYear) {
                                throw "Impossible de programmer un rappel une année à l'avance, désolé !";
                            }
                            if (!isValidDate(scheduleDate_1)) {
                                throw "Date incorrecte";
                            }
                        }
                    }
                    else {
                        day = currentDate.getDate();
                        monthIndex = currentDate.getMonth();
                        year = currentYear;
                    }
                    scheduleDate_1 = new Date(year, monthIndex, day, hours, minutes);
                }
                cronString_1 = getCronString(scheduleDate_1);
                reminderMessage_1 = getMessage(scheduleDate_1, scheduleMessage_1);
                askScheduleMessage = askMessage(reminderMessage_1, authorUsername);
                return [4 /*yield*/, message.channel.send(askScheduleMessage)];
            case 2:
                askSchedule = _a.sent();
                askSchedule.react(thumbsUp);
                askSchedule.react(thumbsDown);
                filter = function (reaction, user) {
                    //filtering the reactions from the user
                    return ([thumbsUp, thumbsDown].includes(reaction.emoji.name) &&
                        user.id === message.author.id);
                };
                askSchedule
                    .awaitReactions(filter, { max: 1, time: 60000, errors: ["time"] }) //awaiting the reactions - remember the time is in milliseconds
                    .then(function (collected) {
                    var reaction = collected.first();
                    if (reaction.emoji.name === thumbsUp) {
                        cron.schedule(cronString_1, function () {
                            message.reply(reminderMessage_1);
                        });
                        var scheduleConfirmed = reminderConfirmation(scheduleDate_1, scheduleMessage_1, authorUsername);
                        message.reply(scheduleConfirmed);
                    }
                    else {
                        message.channel.send("Rappel annulé");
                    }
                })["catch"](function () {
                    message.channel.send("Rappel annulé");
                });
                return [3 /*break*/, 4];
            case 3:
                error_1 = _a.sent();
                console.log(error_1);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
var isBetween = function (x, minimum, maximum) {
    return minimum <= x && x <= maximum;
};
var formattedDate = function (date) {
    return [date.getDate(), date.getMonth() + 1, date.getFullYear()]
        .map(function (n) { return (n < 10 ? "0" + n : "" + n); })
        .join("/");
};
var isValidDate = function (date) {
    return date instanceof Date && !isNaN(date.valueOf());
};
var getCronString = function (date) {
    var day = date.getDate().toString();
    var month = (date.getMonth() + 1).toString();
    var hours = date.getHours().toString();
    var minutes = date.getMinutes().toString();
    return minutes + " " + hours + " " + day + " " + month + " *";
};
//('one two three -- four five --six'.split(' '), { '-': true })
var getArgs = function (command) {
    var c = command.startsWith(prefix)
        ? command.slice(prefix.length + 1)
        : command;
    return minimist(string_argv_1["default"](c));
};
var checkParameters = function (args) {
    return args.t || args.h || (args.d && args.h);
};
var getMessage = function (date, message) {
    var formatDate = formattedDate(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    return "Rappel: " + message + " le " + formatDate + " \u00E0 " + hours + ":" + minutes;
};
var reminderConfirmation = function (date, message, authorUsername) {
    var formatDate = formattedDate(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    return "@" + authorUsername + "\nRappel \"" + message + "\" programm\u00E9 pour le " + formatDate + " \u00E0 " + hours + ":" + minutes + ":" + seconds;
};
var askMessage = function (message, authorUsername) {
    return "@" + authorUsername + " - Programmer le rappel \"" + message + "\" ?";
};
