"use strict";
exports.__esModule = true;
var minimist = require("minimist");
var string_argv_1 = require("string-argv");
function formattedDate(date) {
    return [date.getDate(), date.getMonth() + 1, date.getFullYear()]
        .map(function (n) { return (n < 10 ? "0" + n : "" + n); })
        .join("/");
}
var args = minimist(string_argv_1["default"]('r! "Rappel" -t 30m'));
console.log(args);
