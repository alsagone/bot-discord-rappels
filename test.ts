import minimist = require("minimist");
import process = require("process");
import stringArgv from "string-argv";

function formattedDate(date: Date): string {
  return [date.getDate(), date.getMonth() + 1, date.getFullYear()]
    .map((n) => (n < 10 ? `0${n}` : `${n}`))
    .join("/");
}

const args = minimist(stringArgv('r! "Rappel" -t 30m'));

console.log(args);
