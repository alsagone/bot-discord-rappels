import Discord = require("discord.js");
import cron = require("node-cron");
import minimist = require("minimist");
import moment = require("moment");
import stringArgv from "string-argv";

const botToken = "ODQ1NTkzNjUzNDg5MzAzNTYy.YKjOdQ.G3hx4gVf1Gus_-3it22SXcW4r48";
const client = new Discord.Client();
const prefix = "r!";
const dateRegex: RegExp = /(\d{1,2})\/(\d{1,2})(\/(\d{4}))?/gm;
const timeRegex: RegExp = /(\d{1,2}h)?(\d{1,2})?m?/gm;
const badCommand: string =
  "Commande incorrecte: tapez r! --help pour afficher l'aide :)";
const thumbsUp: string = "👍";
const thumbsDown: string = "👎";

/*  Commandes 

******** Rappel dans X minutes ********
* r! "Mon rappel" -t 30m   -> rappel dans 30 minutes
* r! "Mon rappel" -t 1h    -> rappel dans 1 heure
* r! "Mon rappel" -t 1h30  -> rappel dans 1h30

******** Rappel à une heure définie ********
* r! "Mon rappel" -h 16h25          -> rappel aujourd'hui à 16h25
* r! "Mon rappel" -d DD/MM/YYYY -h 16h    -> rappel le DD/MM/YYYY à 16h00
* r! "Mon rappel" -d DD/MM/YYYY -h 16h30  -> rappel le DD/MM/YYYY à 16h30

******** Liste des rappels ********
* r! -list   -> liste les rappels
*/

client.login(botToken);

client.on("message", async (message) => {
  if (message.author.bot || !message.content.startsWith(prefix)) return;
  const authorUsername = message.author.username;
  const commandBody = message.content.slice(prefix.length);

  try {
    const args = getArgs(commandBody);

    if (!(args.t || args.h || (args.d && args.h))) {
      throw badCommand;
    }

    let scheduleDate: Date;
    let cronString: string;
    const scheduleMessage: string = args._[0];
    let reminderMessage: string;
    let day: number;
    let monthIndex: number;
    let year: number;
    let hours: number = 0;
    let minutes: number = 0;
    let currentDate: Date = new Date();
    const currentYear: number = currentDate.getFullYear();

    if (args.t) {
      const timeMatches: RegExpMatchArray = timeRegex.exec(args.t);

      if (timeMatches === null) {
        throw badCommand;
      }

      if (timeMatches[0].endsWith("h")) {
        hours = parseInt(timeMatches[0].slice(0, -1));

        if (!isBetween(hours, 0, 23)) {
          throw `Horaire incorrect: ${timeMatches[0]}`;
        }

        if (timeMatches.length == 2) {
          minutes = parseInt(timeMatches[1]);

          if (!isBetween(minutes, 0, 59)) {
            throw `Horaire incorrect: ${timeMatches[1]}`;
          }
        }
      } else {
        const m: string = timeMatches[0].endsWith("m")
          ? timeMatches[0].slice(0, -1)
          : timeMatches[0];

        minutes = parseInt(m);
        if (!isBetween(minutes, 0, 59)) {
          throw `Horaire incorrect: ${timeMatches[0]}`;
        }
      }

      scheduleDate = moment(new Date())
        .add(hours, "h")
        .add(minutes, "m")
        .toDate();
    } else if (args.h) {
      const timeMatches: RegExpMatchArray = timeRegex.exec(args.h);

      if (timeMatches === null) {
        throw badCommand;
      }

      if (timeMatches[0].includes("h")) {
        hours = parseInt(timeMatches[0].slice(0, -1));

        if (!isBetween(hours, 0, 23)) {
          throw `Horaire incorrect: ${timeMatches[0]}`;
        }

        if (timeMatches.length == 2) {
          minutes = parseInt(timeMatches[1]);

          if (!isBetween(minutes, 0, 59)) {
            throw `Horaire incorrect: ${timeMatches[1]}`;
          }
        }
      } else {
        const m: string = timeMatches[0].endsWith("m")
          ? timeMatches[0].slice(0, -1)
          : timeMatches[0];

        minutes = parseInt(m);
        if (!isBetween(minutes, 0, 59)) {
          throw `Horaire incorrect: ${timeMatches[0]}`;
        }
      }

      if (args.d) {
        const dateMatches = dateRegex.exec(args.d);

        if (dateMatches === null) {
          throw "Date incorrecte";
        } else {
          day = parseInt(dateMatches[0]);
          monthIndex = parseInt(dateMatches[1]) - 1;
          year =
            dateMatches.length === 3 ? parseInt(dateMatches[2]) : currentYear;

          if (year !== currentYear) {
            throw "Impossible de programmer un rappel une année à l'avance, désolé !";
          }

          if (!isValidDate(scheduleDate)) {
            throw "Date incorrecte";
          }
        }
      } else {
        day = currentDate.getDate();
        monthIndex = currentDate.getMonth();
        year = currentYear;
      }

      scheduleDate = new Date(year, monthIndex, day, hours, minutes);
    }

    cronString = getCronString(scheduleDate);
    reminderMessage = getMessage(scheduleDate, scheduleMessage);

    const askScheduleMessage: string = askMessage(
      reminderMessage,
      authorUsername
    );
    const askSchedule = await message.channel.send(askScheduleMessage);
    askSchedule.react(thumbsUp);
    askSchedule.react(thumbsDown);

    const filter = (reaction, user) => {
      //filtering the reactions from the user
      return (
        [thumbsUp, thumbsDown].includes(reaction.emoji.name) &&
        user.id === message.author.id
      );
    };
    askSchedule
      .awaitReactions(filter, { max: 1, time: 60000, errors: ["time"] }) //awaiting the reactions - remember the time is in milliseconds
      .then((collected) => {
        const reaction = collected.first();

        if (reaction.emoji.name === thumbsUp) {
          cron.schedule(cronString, () => {
            message.reply(reminderMessage);
          });

          const scheduleConfirmed: string = reminderConfirmation(
            scheduleDate,
            scheduleMessage,
            authorUsername
          );
          message.reply(scheduleConfirmed);
        } else {
          message.channel.send("Rappel annulé");
        }
      })
      .catch(() => {
        message.channel.send("Rappel annulé");
      });
  } catch (error) {
    console.log(error);
  }
});

const isBetween = (x: number, minimum: number, maximum: number): boolean => {
  return minimum <= x && x <= maximum;
};

const formattedDate = (date: Date): string => {
  return [date.getDate(), date.getMonth() + 1, date.getFullYear()]
    .map((n) => (n < 10 ? `0${n}` : `${n}`))
    .join("/");
};

const isValidDate = (date: Date): boolean => {
  return date instanceof Date && !isNaN(date.valueOf());
};

const getCronString = (date: Date): string => {
  const day: string = date.getDate().toString();
  const month: string = (date.getMonth() + 1).toString();
  const hours: string = date.getHours().toString();
  const minutes: string = date.getMinutes().toString();

  return `${minutes} ${hours} ${day} ${month} *`;
};

//('one two three -- four five --six'.split(' '), { '-': true })
const getArgs = (command: string) => {
  const c: string = command.startsWith(prefix)
    ? command.slice(prefix.length + 1)
    : command;
  return minimist(stringArgv(c));
};

const checkParameters = (args: any) => {
  return args.t || args.h || (args.d && args.h);
};

const getMessage = (date: Date, message: string) => {
  const formatDate: string = formattedDate(date);
  const hours: number = date.getHours();
  const minutes: number = date.getMinutes();

  return `Rappel: ${message} le ${formatDate} à ${hours}:${minutes}`;
};

const reminderConfirmation = (
  date: Date,
  message: string,
  authorUsername: string
) => {
  const formatDate: string = formattedDate(date);
  const hours: number = date.getHours();
  const minutes: number = date.getMinutes();
  const seconds: number = date.getSeconds();

  return `@${authorUsername}\nRappel "${message}" programmé pour le ${formatDate} à ${hours}:${minutes}:${seconds}`;
};

const askMessage = (message: string, authorUsername: string) => {
  return `@${authorUsername} - Programmer le rappel "${message}" ?`;
};
